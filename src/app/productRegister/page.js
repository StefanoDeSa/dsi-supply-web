"use client";
import DefaultLayout from "@/components/Layouts/DefaultLayout";
import { createProduct } from "@/services/ProductService";
import { findByEmail } from "@/services/SuppliersService";
import { useState } from "react";
import { useSession } from "next-auth/react";
import { createProductSupplier } from "@/services/ProductSupplierService";

export default function ProductRegister() {
  const { data: session } = useSession();
  const emailUser = session?.user?.email;

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();

    const productData = {
      name,
      description,
      category,
    };

    const supplierResponse = await findByEmail(emailUser);
    const productResponse = await createProduct(productData);

    const dataRelation = {
      productId: productResponse.id,
      supplierId: supplierResponse.id,
    }

    const productsupplierResponse = await createProductSupplier(dataRelation)

    setName("");
    setDescription("");
    setCategory("");
  };

  return (
    <DefaultLayout>
      <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
        <div className="border-b border-stroke px-6.5 py-4 dark:border-strokedark">
          <h3 className="font-medium text-black dark:text-white">
            Cadastrar produto
          </h3>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="flex flex-col gap-5.5 p-6.5">
            <div>
              <label className="mb-3 block text-sm font-medium text-black dark:text-white">
                Nome do produto
              </label>
              <input
                value={name}
                onChange={(e) => setName(e.target.value)}
                type="text"
                placeholder="Nome do produto"
                className="w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary"
              />
            </div>

            <div>
              <label className="mb-3 block text-sm font-medium text-black dark:text-white">
                Descrição do produto
              </label>
              <textarea
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                rows={3}
                placeholder="Descrição do produto"
                className="w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary"
              ></textarea>
            </div>

            <div>
              <label className="mb-3 block text-sm font-medium text-black dark:text-white">
                Categoria
              </label>
              <input
                value={category}
                onChange={(e) => setCategory(e.target.value)}
                type="text"
                placeholder="Categoria"
                className="w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary"
              />
            </div>
            <div>
              <div className="inline-flex items-center justify-center rounded-md bg-primary px-10 py-4 text-center font-medium text-white hover:bg-opacity-90 lg:px-8 xl:px-10">
                <input type="submit" value="Cadastrar"></input>
              </div>
            </div>
          </div>
        </form>
      </div>
    </DefaultLayout>
  );
}
