"use client";
import "@/css/satoshi.css";
import "@/css/style.css";
import { useState } from "react";
import Providers from "@/components/Providers/Providers";

export default function RootLayout({ children }) {
  const [sidebarOpen, setSidebarOpen] = useState(false);

  return (
    <html lang="en">
      <body suppressHydrationWarning={true}>
        <div className="dark:bg-boxdark-2 dark:text-bodydark">
          <Providers>
            {children}
          </Providers>
        </div>
      </body>
    </html>
  );
}
