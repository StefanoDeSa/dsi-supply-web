export async function createProductSupplier(dados) {
  try {
    const response = await fetch("http://localhost:3001/api/productsupplier/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        productId: dados.productId,
        supplierId: dados.supplierId,
      }),
    });

    if (!response.ok) {
      throw new Error("Problema com a requisição devido a rede");
    }

    const data = await response.json();
    console.log(data);
  } catch (error) {
    console.error("Houve um problema com a operação de busca:", error);
  }
}
