export async function createProduct(productData) {
  try {
    const response = await fetch("http://localhost:3001/api/products/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: productData.name,
        description: productData.description,
        category: productData.category,
      }),
    });

    if (!response.ok) {
      throw new Error("Problema com a requisição devido a rede");
    }

    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Houve um problema com a operação de busca:", error);
  }
}
