export async function createSupplier(email) {
  try {
    const response = await fetch("http://localhost:3001/api/suppliers/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email: email }),
    });

    if (!response.ok) {
      throw new Error("Problema com a requisição devido a rede");
    }

    const data = await response.json();
    console.log(data);
  } catch (error) {
    console.error("Houve um problema com a operação de busca:", error);
  }
}

export async function findByEmail(email) {
  try {
    const response = await fetch(
      `http://localhost:3001/api/suppliers/email/${email}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    if (!response.ok) {
      throw new Error("Problema com a requisição devido a rede");
    }

    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Houve um problema com a operação de busca:", error);
  }
}

export async function updateSupplier(dados, id) {
  try {
    const response = await fetch(`http://localhost:3001/api/suppliers/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        business_name: dados.name,
        cnpj: dados.cnpj,
      }),
    });

    if (!response.ok) {
      throw new Error("Problema com a requisição devido a rede");
    }

    const data = await response.json();
    console.log(data);
  } catch (error) {
    console.error("Houve um problema com a operação de busca:", error);
  }
}
